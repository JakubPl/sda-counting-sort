public class CountingSortMain {
    public static void main(String[] args) {
        int[] numbersToSort = {4, 5, 10, 20, 3, 1, 20, 10, 5};
        int[] freqArray = new int[21];

        for(int i = 0; i < numbersToSort.length; i ++) {
            int numberToCalculateFreq = numbersToSort[i];
            //freqArray[numberToCalculateFreq] = freqArray[numberToCalculateFreq] + 1;
            freqArray[numberToCalculateFreq] ++;
        }

        for(int j = 0; j < freqArray.length; j ++) {
            int frequencyOfJ = freqArray[j];
            for(int a = 0; a < frequencyOfJ; a ++) {
                System.out.println(j);
            }
        }
    }
}
